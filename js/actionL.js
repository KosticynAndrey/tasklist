moment.locale('ru');

var tasks = [
    {
        id: 1,
        title: 'проснуться',
        deadline: 1556092800000,
        is_complite: false
    },
    {
        id: 2,
        title: 'умыться',
        deadline: 1579054800000,
        is_complite: false
    },
];

const container = document.getElementsByClassName('main-block')[0];

function renderTaskList(business) {
    container.innerHTML +=
        `
    <div class="main-block__item">
            <input type="checkbox">
            <div class="main-block__item-number">${business.id}</div>
            <div class="main-block__item-deadline">${convertToDate(business.deadline)}</div>
            <div class="main-block__item-todo">${business.title}</div>
            <div class="main-block__item-add" onclick="addTask(${business.id})"></div>
            <div class="main-block__item-delete" id=${business.id}></div>
        </div>
    `;
}

/* 
    <div class="main-block__item-delete" onclick="deleteTask(${business.id})"></div>

 */



function convertToDate(data) {
    return moment(data).format('lll');
}

function renderAll() {
    container.innerHTML = '';
    tasks.forEach(business => {
        renderTaskList(business);
    });
}

const modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
modalWrapper.addEventListener('click', hideDelModal);
document.getElementsByClassName('modal-delete')[0].addEventListener('click', stopProp);
document.getElementsByClassName('close-modal')[0].addEventListener('click', hideDelModal);
document.getElementsByClassName('no')[0].addEventListener('click', hideDelModal);
var taskId;
renderAll();

const del = document.getElementsByClassName('main-block__item-delete');
const yes = document.getElementsByClassName('yes')[0].addEventListener('click', deleteTask);
for (var i = 0; i < del.length; i++) {
    del[i].addEventListener('click', showDelModal);
}

document.getElementById('confirm').addEventListener('click', deleteTask);

function stopProp(event) {
    event.stopPropagation();
}

function showDelModal() {
    taskId = this.id;
    console.log(this.id);
    modalWrapper.style.display = 'flex';
}
function hideDelModal() {
    modalWrapper.style.display = 'none';
}

function deleteTask() {
    const task = tasks.find(item => item.id === +taskId);
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    hideDelModal();
    renderAll();
    const del = document.getElementsByClassName('main-block__item-delete');
    for (var i = 0; i < del.length; i++) {
        del[i].addEventListener('click', showDelModal);
    }
}
